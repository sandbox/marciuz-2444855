<?php
/**
 * @file
 * Lightsocialbuttons base class.
 */

/**
 * Functions of the Stat sub-module.
 */
class LightSocialButtons {

  /**
   * The URL.
   *
   * @var string
   */
  public $resource;

  /**
   * The URL encoded.
   *
   * @var string
   */
  public $resourceEncoded;

  /**
   * The md5 hash of the URL.
   *
   * This hash is used as primary key in the lightsocialbuttons table.
   *
   * @var string
   */
  public $hash;

  /**
   * Constructor: expect a URL as parameter.
   *
   * @param string $resource
   *   The URL.
   */
  public function __construct($resource) {

    $this->resource = $resource;
    $this->resourceEncoded = urlencode($resource);
    $this->hash = md5($this->resource);
  }

  /**
   * Get the total shares from Facebook of a $resource URL.
   *
   * @return mixed
   *   Int|False
   */
  public function getFacebook() {

    $path = "http://graph.facebook.com/fql?q=SELECT%20url%2C%20normalized_url%2C%20share_count%2C%20like_count%2C%20comment_count%2C%20total_count%2Ccommentsbox_count%2C%20comments_fbid%2C%20click_count%20FROM%20link_stat%20WHERE%20url%3D'" . $this->resourceEncoded . "'";
    $o = (array) $this->sendByGet($path);

    if (isset($o['data'][0]['total_count'])) {
      return $o['data'][0]['total_count'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the total shares from Twitter of a $resource URL.
   *
   * @return mixed
   *   Int|False
   */
  public function getTwitter() {

    $path = "http://cdn.api.twitter.com/1/urls/count.json?url=" . $this->resourceEncoded;
    $o = $this->sendByGet($path);
    if (isset($o['count'])) {
      return $o['count'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the total shares from GooglePlus of a $resource URL.
   *
   * @return mixed
   *   Int|False
   */
  public function getGoogleplus() {

    $data_post = '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'
          . $this->resource
          . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]';

    $path = 'https://clients6.google.com/rpc';
    $o = $this->sendByPost($path, $data_post);

    if (isset($o[0]['result']['metadata']['globalCounts']['count'])) {
      return intval($o[0]['result']['metadata']['globalCounts']['count']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the total shares from Pinteres of a $resource URL.
   *
   * @return mixed
   *   Int|False
   */
  public function getPinterest() {

    $path = "http://api.pinterest.com/v1/urls/count.json?url=" . $this->resourceEncoded;
    $return_data = $this->sendByGet($path, TRUE);
    $json = preg_replace('/^receiveCount\((.*)\)$/', "\\1", $return_data);

    $o = json_decode($json, TRUE);

    if (isset($o['count'])) {
      return $o['count'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the total shares from Linkedin of a $resource URL.
   *
   * @return mixed
   *   Int|False
   */
  public function getLinkedin() {

    $path = 'https://www.linkedin.com/countserv/count/share?format=json&url=' . $this->resourceEncoded;
    $o = $this->sendByGet($path);

    if (isset($o['count'])) {
      return $o['count'];
    }
    else {
      return FALSE;
    }

  }

  /**
   * Get data from a URL with POST.
   *
   * @param string $url
   *   The URL.
   * @param array $post_fields
   *   Array of (keys => $values).
   * @param bool $raw
   *   The function can return a JSON object or a raw output.
   *
   * @return mixed
   *   Raw text | JSON encoded response (default).
   */
  private function sendByPost($url, array $post_fields, $raw = FALSE) {

    $options = array(
      'method' => 'POST',
      'data' => $post_fields,
      'timeout' => 15,
      'headers' => array('Content-Type' => 'Content-type: application/json'),
    );

    $rq = drupal_http_request($url, $options);
    $result = (isset($rq->data)) ? $rq->data : NULL;

    if ($raw) {
      return $result;
    }
    else {
      return json_decode($result, TRUE);
    }
  }

  /**
   * Get data from an URL with GET.
   *
   * @param string $url
   *   The URL.
   * @param bool $raw
   *   The function can return a JSON object or a raw output.
   *
   * @return mixed
   *   Raw text | JSON encoded response (default).
   */
  private function sendByGet($url, $raw = FALSE) {

    $options = array(
      'method' => 'GET',
      'timeout' => 15,
      'headers' => array('Content-Type' => 'Content-type: application/json'),
    );

    $rq = drupal_http_request($url, $options);
    $result = (isset($rq->data)) ? $rq->data : NULL;

    if ($raw) {
      return $result;
    }
    else {
      return json_decode($result, TRUE);
    }
  }

  /**
   * Get a record from cache.
   *
   * @param bool $not_expired
   *   The expired/not expired parameter.
   *
   * @return mixed
   *   Db fetch assoc number.
   */
  public function getCache($not_expired = TRUE) {

    $cache_time = intval(variable_get('lightsocialbuttons_settings_cache_expire', 1800));

    $max_time = (time() - $cache_time);

    $query = db_select('lightsocialbuttons', 'lsb')
            ->fields('lsb')
            ->condition('hurl', $this->hash, '=');

    if ($not_expired) {
      $query->condition('last_timestamp', $max_time, '>');
    }

    $result = $query->execute()->fetchAssoc();

    return $result;
  }

  /**
   * Set the cache.
   *
   * @param array $share_data
   *   The data of social networks count.
   * @param int $nid
   *   The nid of the node (optional).
   */
  public function setCache(array $share_data, $nid = NULL) {

    foreach ($share_data as $k => $v) {
      $data[$k] = (int) $v;
    }

    $data['tot'] = array_sum($share_data);
    $data['url'] = $this->resource;
    $data['last_timestamp'] = time();
    $data['hurl'] = $this->hash;
    if (intval($nid) > 0) {
      $data['nid'] = $nid;
    }

    $res = db_merge('lightsocialbuttons')
      ->key(array('hurl' => $this->hash))
      ->fields($data)
      ->execute();

    return $res;

  }

  /**
   * Show thousand instead of the integer number.
   *
   * @param int $n
   *   The shares.
   * @param bool $is_active
   *   Enable/Disable the function.
   *
   * @return mixed
   *   int|string
   */
  public static function showk($n, $is_active = TRUE) {

    if (!$is_active) {
      return $n;
    }

    $n = intval($n);

    if ($n >= 1000) {
      return round($n / 1000, 1) . "k";
    }
    else {
      return $n;
    }
  }

  /**
   * Get all the data and put in the cache.
   *
   * @param int $nid
   *   Node id.
   *
   * @return bool
   *   The set cache results.
   */
  public function getCountsAll($nid = NULL) {

    $data['facebook'] = $this->getFacebook();
    $data['twitter'] = $this->getTwitter();
    $data['googleplus'] = $this->getGoogleplus();
    $data['linkedin'] = $this->getLinkedin();
    $data['pinterest'] = $this->getPinterest();

    return $this->setCache($data, $nid);
  }

}

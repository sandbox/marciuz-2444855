<?php
/**
 * @file
 * Lightsocialbuttons Statistics class.
 */

/**
 * Functions of the Stat sub-module.
 */
class LightSocialButtonsStats extends LightSocialButtons {

  /**
   * Get the last count of an hashed URL.
   *
   * @return array
   *   The statistics of social buttons
   */
  public function lastCount() {

    $query = db_select('lightsocialbuttons_stats', 'lsbs')
            ->fields('lsbs', array('shday', 'tot'))
            ->condition('hurl', $this->hash, '=')
            ->orderBy('shday', 'DESC')
            ->range(0, 1);

    $result = $query->execute()->fetchAssoc();

    return $result;
  }

  /**
   * Write a record in the stats table.
   *
   * @param array $data
   *   The number of share by social network.
   *
   * @return MergeObject
   *   The Db merged object
   */
  public function writeLog(array $data) {

    unset($data['url']);

    $data['tot'] = $data['facebook']
                + $data['twitter']
                + $data['googleplus']
                + $data['linkedin']
                + $data['pinterest'];

    $data['hurl'] = $this->hash;

    if ($data['nid'] == 0) {
      $data['nid'] == NULL;
    }

    $res = db_merge('lightsocialbuttons_stats')
      ->key(array('hurl' => $this->hash, 'shday' => $data['shday']))
      ->fields($data)
      ->execute();

    return $res;
  }

  /**
   * Get statistics from NID.
   *
   * @param int $nid
   *   The nid for the statistics.
   *
   * @return object
   *   The statistics
   */
  public function getStats($nid) {

    $query = db_select('lightsocialbuttons_stats', 'lsbs')
            ->fields('lsbs')
            ->condition('nid', $nid, '=')
            ->orderBy('shday', 'ASC');

    $ex = $query->execute();

    while ($obj = $ex->fetchObject()) {
      $results[$obj->shday] = $obj;
    }

    return $results;
  }

}

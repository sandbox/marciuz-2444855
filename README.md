# Light Social Buttons
Drupal module that allow the creation of social media buttons with dynamic counters and Views integration


## Current Mantainer

Author and current maintainer: M.Marcello Verona <https://www.drupal.org/user/314554>
Co-maintainer: Gian Mario Mereu <https://www.drupal.org/user/1674394>


## Description

The module allows to create a light social buttons integration, without the embedded iframe or external javascript code and show the counters from the following service:

* Facebook
* Google Plus
* Twitter
* Linkedin
* Pinterest

The module use the javascript API from the services to get the number of share. For the Google Plus service is used an internal call with POST parameter.

## Submodules:

### Statistics 
The sub-module lightsocialbuttons_stats permits to create an history of the contents shares and provide a nice visualization of the growth.

### Views integration
The module lightsocialbuttons_views permits to create a "most shared contents" block.
A default view with all the contents and the share counter is provided.

### Batch Import (alpha)
An experimental module named lightsocialbuttons_batch allows to get all the informations from the social media about your website's page sharing.


## INSTALLING

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

You can find the pre-build blocks "Social buttons" and "Social buttons (alternative)" in the page of administration as usual at admin/structure/block.

Once Light Social Buttons is installed and enabled, you can configure the services and adjust the settings for your
site's at admin/config/media/lightsocialbuttons.

In order to use the views integration you should enable the sub-module lightsocialbuttons_views 

## Credits
The module was inspired from this articles by Craig Buckler 
http://www.sitepoint.com/social-sharing-hidden-costs/
http://www.sitepoint.com/social-media-button-links/ 

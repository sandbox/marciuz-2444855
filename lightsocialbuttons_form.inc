<?php
/**
 * @file
 * Lightsocialbuttons forms and themes.
 */

/**
 * Theme function for lightsocialbuttons_form().
 */
function theme_lightsocialbuttons_form($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['example_items'] array.
  foreach (element_children($form['services']) as $service) {

    $form['services'][$service]['weight']['#attributes']['class'] = array('service-item-weight');

    $rows[] = array(
      'data' => array(

        drupal_render($form['services'][$service]['service'])
        . ' '
        . drupal_render($form['services'][$service]['active']),
        drupal_render($form['services'][$service]['weight']),
      ),

      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'. This will add the 'draggable'
      // class to the <tr> element for that row when the final table is
      // rendered.
      'class' => array('draggable'),
    );
  }

  $header = array(t('Services'), t('Weight'));
  $table_id = 'service-items-table';

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function in order to add the
  // tabledrag.js goodness onto our page.
  //
  // For a basic sortable table, we need to pass it:
  // - the $table_id of our <table> element,
  // - the $action to be performed on our form items ('order'),
  // - a string describing where $action should be applied ('siblings'),
  // - and the class of the element containing our 'weight' element.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'service-item-weight');

  return $output;
}

/**
 * Page callback.
 */
function lightsocialbuttons_form($form, &$form_state) {

  $form = array();

  $form['services']['#tree'] = TRUE;

  $result = variable_get('lightsocialbuttons_services');

  foreach ($result as $k => $item) {

    $form['services'][$item['service']] = array(

      // We'll use a form element of type '#markup' to display the item name.
      'active' => array(
        '#type' => 'checkbox',
        '#title' => t('@service share', array('@service' => $item['service'])),
        '#default_value' => $item['active'],
        '#description' => t('Add @service to allowed butttons', array('@service' => $item['service'])),
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $item['weight'],
        '#delta' => 10,
        '#title_display' => 'invisible',
      ),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function lightsocialbuttons_form_submit($form, &$form_state) {

  $new_services = array();
  $ss = array();

  foreach ($form_state['values']['services'] as $service => $item) {
    $new_services[] = array(
      'service' => $service,
      'weight' => $item['weight'],
      'active' => $item['active'],
    );

    $ss[] = $item['weight'];
  }

  array_multisort($ss, SORT_ASC, $new_services);

  variable_set('lightsocialbuttons_services', $new_services);

}


/**
 * Page callback.
 */
function lightsocialbuttons_option_form($form, &$form_state) {

  $form = array();

  $config = _lightsocialbuttons_config();

  $form['appearance'] = array(
    '#type' => 'fieldset',
    '#title' => ('Appearance'),
  );

  $form['appearance']['lightsocialbuttons_settings_layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout style'),
    '#options' => array(0 => t('Orizontal'), 1 => t('Vertical')),
    '#default_value' => variable_get('lightsocialbuttons_settings_layout', 0),
    '#description' => t('Normally you should use Orizontal in the main contents and Vertical in a sidebar'),
  );

  $form['appearance']['lightsocialbuttons_settings_css'] = array(
    '#type' => 'select',
    '#title' => t('CSS'),
    '#options' => array('default' => t('default')),
    '#default_value' => variable_get('lightsocialbuttons_settings_css', 'default'),
  );

  $form['appearance']['lightsocialbuttons_settings_showk'] = array(
    '#type' => 'checkbox',
    '#title' => t('Shorten big numbers'),
    '#default_value' => variable_get('lightsocialbuttons_settings_showk', TRUE),
    '#description' => t('Show the big numbers with a shorten display: e.g., "1.5K" instead of "1512"'),
  );

  $form['appearance']['lightsocialbuttons_settings_animation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Animate counters'),
    '#default_value' => variable_get('lightsocialbuttons_settings_animation', TRUE),
    '#description' => t('When the counters updates use a fast animation'),
  );

  $form['lightsocialbuttons_settings_twitter_via'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#title' => t('Your twitter name'),
    '#default_value' => variable_get('lightsocialbuttons_settings_twitter_via', NULL),
    '#description' => t('If this field will be filled, clicking on the twitter button a "via @yourname" will be added at the end of the tweet - (see also the !twdoc)', array('!twdoc' => l(t('Twitter documentation'), 'https://dev.twitter.com/web/tweet-button/parameters'))),
  );

  $form['lightsocialbuttons_settings_cache_expire'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Cache time'),
    '#default_value' => variable_get('lightsocialbuttons_settings_cache_expire', 1800),
    '#description' => t('Internal cache for counters (in seconds)'),
  );

  $form['lightsocialbuttons_settings_debugurl'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#title' => t('Debug URL'),
    '#default_value' => variable_get('lightsocialbuttons_settings_debugurl', ''),
    '#description' => t('Url test for debug purpose'),
  );

  return system_settings_form($form);
}
